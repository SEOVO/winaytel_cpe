# -*- coding: utf-8 -*-

from odoo import models, fields, api
import requests

class CPE_Wizard(models.TransientModel):
    _name = 'cpe.wizard'
    _description = "Consulta CPE"
    ruc       = fields.Char(required=True,default="20602933858")
    documento = fields.Selection([('01','Factura'),('03','Boleta')],default='01',required=True)
    serie     = fields.Char(required=True,string="Serie del Documento",default="E001")
    numero    = fields.Integer(required=True,string="N° Documento",default="21")
    fecha     = fields.Date(required=True,string="fecha_de_emision", default="2020-07-10")
    total     = fields.Float(default="1050.00")
    @api.multi
    def get_cpe(self):
        data = {}
        for record in self:
            docu = ''
            if record.documento == '01':
                docu = 'Factura'
            if record.documento == '03':
                docu = 'Boleta'

            url = "https://apiperu.dev/api/cpe"
            payload = "{\n \"ruc_emisor\":"+str('"'+record.ruc+'"')+",\n \"codigo_tipo_documento\":"+str('"'+record.documento+'"')+",\n \"serie_documento\":"+str('"'+record.serie+'"')+",\n \"numero_documento\":"+str('"'+str(record.numero)+'"')+",\n\"fecha_de_emision\": "+str('"'+str(record.fecha)+'"')+",\n\"total\":"+str('"'+str(record.total)+'"')+"\n}"
            headers = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer 0dbeba144aea0fd77d8da07f8294985ce38e59612939af76d924ff9aaaead4c4'
             }
            cd = True
            i = 0
            while cd == True:
                response = requests.request("POST", url, headers=headers, data = payload)
                if 'data' in response:
                    cd = False
                if i <= 4:
                    cd = False
                i += 1
            response = requests.request("POST", url, headers=headers, data = payload)
            response=response.json()
            datax = ''
            empresa_estado     = ''
            empresa_condicion  = ''
            comprobante_estado = ''
            if 'data' in response:
                empresa_estado     = response['data']['empresa_estado_description'],
                empresa_condicion  = response['data']['empresa_condicion_descripcion'],
                comprobante_estado = response['data']['comprobante_estado_descripcion'], 
                done  = 0
            else:
                datax = response['message']
                done  = 1
            data = {
                'response'  : datax,
                'payload'   : payload,
                'ruc'       : record.ruc, 
                'docu'      : docu,
                'serie'     : record.serie,
                'numero'    : record.numero,
                'fecha'     : record.fecha,
                'total'     : record.total,
                'done'      : done,
                'empresa_estado'     : empresa_estado,
                'empresa_condicion'  : empresa_condicion,
                'comprobante_estado' : comprobante_estado,

            }
        return self.env.ref('winaytel_cpe.reporte_cpe').report_action(self, data=data)

